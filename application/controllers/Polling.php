<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Polling extends CI_Controller {

	public function __construct()
	{
	   
		parent::__construct();
		#load models		
		$this->load->helper('url');
		$this->load->model('login_model');	
		$this->load->model('polling_model');	
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
 	}

	public function index(){
		try { 
		 $user_data = $this->session->userdata('logged_in');
		 $data['polling_result'] = [];
		 $data['name']    = $user_data['firstname'];
		 $data['opinion'] = $this->polling_model->get_all_opinion();		 

		}catch (Exception $e) {
		  //alert the user.
		  var_dump($e->getMessage());
		}
		$this->load->view ('polling', $data);		
	}
}
