<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pollingresult extends CI_Controller {

	public function __construct()
	{
	   
		parent::__construct();
		#load models		
		$this->load->helper('url');
		$this->load->model('login_model');	
		$this->load->model('polling_model');	
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
 	}

	public function index(){

		try {

			 $user_data = $this->session->userdata('logged_in');
			 $result=$this->input->post();
			 $j=0;
			 foreach ($result as $key => $res) {

			 	$implode = explode('_', $key);
			 	$opinion_id = $implode[1];

			 	$get_ans = $this->polling_model->get_answer($opinion_id);
			 	if ($get_ans==$res) {
			 		$j++;
			 	}

			 }

		  $data['user_id']             = $user_data['user_id'];
		  $data['name']                = $user_data['firstname'];
		  #Calculation Part
	      $data['total_ques']          = count($result);
	      $data['total_write_ans']     = $j;
	      $data['total_wrong_ans']     = count($result) - $j;
	      $data['average']             = ($j*100)/count($result);
	      #End

		}catch (Exception $e) {
		  //alert the user.
		  var_dump($e->getMessage());
		}
		$this->load->view ('polling_result', $data);
		
	}
}
