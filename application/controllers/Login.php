<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
	   
		parent::__construct();
		$this->load->helper('url');
		#load models		
		$this->load->model('login_model');		
		$this->load->library('session');
		//$this->load->library('database');
		$this->load->database();

 	}

	public function login(){

			$post = $this->input->post();
			$row_count = $this->login_model->login_check($post);

	}
	
	function logout()
    {
        $user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
                $this->session->unset_userdata($key);
            }
        }
	    $this->session->sess_destroy();
	    redirect('Welcome','refresh');
    }
}
