<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class login_model extends CI_Model {

	public function login_check($postdata)
	{

        // user input
        $email = $postdata['email'];
        $password = $postdata['password'];
        // Prep the query
        $this->db->where('email', $email);
        $this->db->where('password', $password);
        // Run the query
        $query = $this->db->get('users');
        // Let's check if there are any results
        if($query->num_rows() > 0)
        {
            // If there is a user, then create session data
            $row = $query->row();
            $userdata = array(
                    'user_id' => $row->user_id,
                    'firstname' => $row->firstname,
                    'email' => $row->email
                    );

            $this->session->set_userdata('logged_in',$userdata);
            echo true;
        }
        // If the previous process did not validate
        // then return false.
        echo false;
    }



	
}
