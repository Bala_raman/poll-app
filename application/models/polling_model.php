<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class polling_model extends CI_Model {

	public function get_all_opinion()
	{

       
           return $get_opinion = $this->db->select('opinion_id,opinion,answer1, answer2,answer3,answer4,is_right')
                                          ->from('opinion')
                                          ->where('question_status',1)
                                          ->get()->result_array();
    }

    public function get_answer($opinion_id)
	{

       
           return $get_opinion = $this->db->select('is_right')->from('opinion')->where('opinion_id', $opinion_id)->get()->row()->is_right;
    }

	
}
