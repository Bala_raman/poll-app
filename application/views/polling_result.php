<div style="background: #fff; box-shadow: 0px -2px 5px 2px #f1f1f1; padding: 10px; width: 300px; margin: 0 auto; height: 250px;">
	<h3>Welcome <span style="color: #f00; font-weight: bold;"><?=$name?></span> Your result below details.</h3>
	<a href="<?php echo base_url();?>Login/logout" style="background: green; padding: 8px 12px; color: #fff; font-size: 14px; font-weight: bold; text-decoration: none; border-radius: 8px;">Logout</a><br />
    <ul style="padding: 0; list-style: none;">
        <li style="padding: 5px;">Total Question: <b><?=$total_ques?></b></li>
        <li style="padding: 5px;">Right Answer : <b><?=$total_write_ans?></b></li>
        <li style="padding: 5px;">Wrong Answer : <b><?=$total_wrong_ans?></b></li>
        <li style="padding: 5px;">Average      : <b><?=$average?></b></li>
    </ul>
</div>