-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 17, 2019 at 01:46 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zendcode`
--

-- --------------------------------------------------------

--
-- Table structure for table `opinion`
--

CREATE TABLE `opinion` (
  `opinion_id` int(11) NOT NULL,
  `opinion` text NOT NULL,
  `answer1` tinytext NOT NULL,
  `answer2` tinytext NOT NULL,
  `answer3` tinytext NOT NULL,
  `answer4` tinytext NOT NULL,
  `is_right` enum('A','B','C','D') NOT NULL,
  `question_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `opinion`
--

INSERT INTO `opinion` (`opinion_id`, `opinion`, `answer1`, `answer2`, `answer3`, `answer4`, `is_right`, `question_status`) VALUES
(1, 'Which of the following is correct about PHP?', 'PHP can access cookies variables and set cookies.', 'Using PHP, you can restrict users to access some pages of your website.', 'It can encrypt data.', 'All of the above.', 'D', 1),
(2, 'Which of the following operator is used to concatenate two strings?', '.', '+', 'append', 'None of the above.', 'A', 1),
(3, 'Which of the following function is used to get environment variables in PHP?', 'search()', 'environment()', 'env()', 'getenv()', 'D', 1),
(4, 'Which of the following is used to check that a cookie is set or not?\r\n\r\n', ' getcookie() function', ' $_COOKIE variable\r\n\r\n', 'isset() function\r\n\r\n', 'None of the above.\r\n\r\n', 'C', 1),
(5, 'Which of the following provides access to the uploaded file in the temporary directory on the web server?', ' $_FILES[\'file\'][\'tmp_name\']', '$_FILES[\'file\'][\'name\']\r\n\r\n', '$_FILES[\'file\'][\'size\']', '$_FILES[\'file\'][\'type\']', 'A', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `email`, `password`, `status`, `created_at`) VALUES
(1, 'balaraman', 'bala@gmail.com', 'Pass123@', 1, '2019-06-15 08:29:18'),
(2, 'kumar', 'veera@chennovate.com', 'Pass123@', 1, '2019-06-15 08:29:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `opinion`
--
ALTER TABLE `opinion`
  ADD PRIMARY KEY (`opinion_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `opinion`
--
ALTER TABLE `opinion`
  MODIFY `opinion_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
